import serial
import rospy
from std_msgs.msg import Float32

def talker():
	pub = rospy.Publisher('kecepatan', Float32, queue_size=10)
	rospy.init_node('gps', anonymous=True)
	ser = serial.Serial('/dev/ttyACM0', baudrate = 9600, timeout=1)
	while not rospy.is_shutdown():
		arduinoData = ser.readline()[:-2]
		a = arduinoData.decode()
 		if a != '':
    		a = float(a)
    		pub.publish(a)

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException: pass