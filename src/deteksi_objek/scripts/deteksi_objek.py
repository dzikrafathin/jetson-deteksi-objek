#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image, LaserScan
from std_msgs.msg import String, Float32
from cv_bridge import CvBridge, CvBridgeError

import cv2
import numpy as np

import jetson.inference
import jetson.utils

rospy.init_node('deteksi_objek')

bridge = CvBridge()
gambar = np.zeros((480,640), np.uint8)
daftar_jarak = []
rate = rospy.Rate(30)

net = jetson.inference.detectNet("ssd-mobilenet-v2", threshold=0.5)

def simpan_jarak_cb(data):
    global daftar_jarak
    daftar_jarak = data.ranges

def simpan_gambar_cb(data):
    global gambar
    try:
        gambar = bridge.imgmsg_to_cv2(data, 'bgr8')
    except CvBridgeError as e:
        print(e)

rospy.Subscriber('webcam/image_raw',Image,simpan_gambar_cb)
rospy.Subscriber('scan',LaserScan,simpan_jarak_cb)

nama_objek_pub = rospy.Publisher('objek/nama',String,queue_size=10)
jarak_objek_pub = rospy.Publisher('objek/jarak',Float32,queue_size=10)

while not rospy.is_shutdown():

    gambar_rgba = cv2.cvtColor(gambar, cv2.COLOR_BGR2RGBA)
    gambar_cuda = jetson.utils.cudaFromNumpy(gambar_rgba)

    terdeteksi = net.Detect(gambar_cuda, 640, 480)

    for objek in terdeteksi:

        jarak_objek = 0.0
        derajat_objek = (60.0 / 640.0) * objek.Center[0]

        if len(daftar_jarak) > 0:
            derajat_objek = (60.0 - derajat_objek) + 60.0
            jarak_objek = daftar_jarak[int(derajat_objek)]
            
            data_jarak_objek = Float32()
            data_jarak_objek.data = jarak_objek
            jarak_objek_pub.publish(data_jarak_objek)


        nama_objek = net.GetClassDesc(objek.ClassID)

        data_nama_objek = String()
        data_nama_objek.data = nama_objek
        nama_objek_pub.publish(data_nama_objek)

        (x1, y1) = (
            int(objek.Left),
            int(objek.Top)
        )

        (x2, y2) = (
            int(objek.Right),
            int(objek.Bottom)
        )

        cv2.rectangle(
            gambar,
            (x1,y1),
            (x2,y2),
            (0,0,255),
            2
        )

        cv2.putText(
            gambar,
            nama_objek,
            (x1,y1),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (0,255,0),
            2
        )

        cv2.putText(
            gambar,
            str(jarak_objek),
            (
                int(objek.Center[0]),
                int(objek.Center[1])
            ),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (150,50,0),
            2
        )

    cv2.imshow('Pendeteksian Objek', gambar)

    if cv2.waitKey(1) & 0xff == ord('q'):
        break
    
    rate.sleep()

cv2.destroyAllWindows()

