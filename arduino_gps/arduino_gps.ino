#include <SoftwareSerial.h>
#include <TinyGPS++.h>

static const int RXPin = 12, TXPin = 10;
static const uint32_t GPSBaud = 115200;

// The TinyGPS++ object
TinyGPSPlus gps;
int spd =0;


SoftwareSerial ss(RXPin, TXPin);
void setup() {
  Serial.begin(9600);
  ss.begin(GPSBaud);
  delay(100);
}

void displayInfo()
{

  if (gps.speed.isValid())
  {
    spd = gps.speed.kmph();
  }
}


void loop() {
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();
      Serial.println(spd);
 delay(100);

}